#!/usr/bin/python
import prman
import getpass, time, math

############################### Util Functions ####################################

def createGnomon(ri, length = 1, thickness = 0.01):
	# NFO: Creates an XYZ gnomon in the scene, helping to visualize orientation axes.
	'''
	ri : prman.Ri()
	length: length of the axis cylinders
	thickness : radius of the axis cylinders
	'''
	gnomon = ri.ObjectBegin()
	
	# X
	ri.Bxdf("PxrConstant", "constant1",  {'color emitColor' : [1,0,0]})
	ri.TransformBegin()
	ri.Rotate(90, 0,1,0)
	ri.Cylinder(thickness, 0, length, 360)
	ri.TransformEnd()
	
	# Y
	ri.Bxdf("PxrConstant", "constant2",  {'color emitColor' : [0,1,0]})
	ri.TransformBegin()
	ri.Rotate(-90, 1,0,0)
	ri.Cylinder(thickness, 0, length, 360)
	ri.TransformEnd()

	# Z
	ri.Bxdf("PxrConstant", "constant3",  {'color emitColor' : [0,0,1]})
	ri.TransformBegin()
	ri.Cylinder(thickness, 0, length, 360)
	ri.TransformEnd()

	ri.ObjectEnd()
	ri.ObjectInstance(gnomon)

############################### initial setup #####################################
ri = prman.Ri()
ri.Option('rib', {'string asciistyle': 'indented'})

# Output
filename = 'assignment1.rib' # filename = '__render'
ri.Begin(ri.RENDER) # ri.Begin(filename)

# Archive options
ri.Option('searchpath', {'string archive':'../geo/'})
ri.ArchiveRecord(ri.COMMENT, 'File ' + filename)
ri.ArchiveRecord(ri.COMMENT, 'Author ' + getpass.getuser())
ri.ArchiveRecord(ri.COMMENT, 'Date: ' + time.ctime(time.time()))

############################### camera setup #####################################
ri.Display("assignment1.tif", "it", "rgba")
ri.Format(1280, 720, 1.0)                                             
ri.Projection("perspective", {"fov" : 54})
# dof 1
ri.DepthOfField(5.6, 0.06, 6.5)
# dof 2
#ri.DepthOfField(5.6, 0.2, 1.2)
ri.Shutter(0, 0.4)

# camera 1 (for image 1)

ri.Rotate(28, 1, 0, 0)
ri.Rotate(-19, 0, 1, 0)
ri.Translate(-1.756, 4.1, 6.367)

# camera 2 (for image 2)
'''
ri.Rotate(46.5, 1, 0, 0)
ri.Rotate(-47, 0, 1, 0)
ri.Translate(-2.92, 3.3, 2.82)
'''

ri.Scale(1, 1, -1)

############################### hider / integrator ##############################
ri.Hider('raytrace', {	'string integrationmode' : 'path',
						'int incremental' : 1,						
						'int maxsamples' : 512})
ri.Integrator('PxrDefault', 'integrator')
ri.Integrator('PxrDirectLighting', 'integrator')
ri.Integrator('PxrPathTracer', 'integrator')
ri.Integrator('PxrVCM', 'integrator')

############################### make textures ####################################
# countries 1 (for image 1)
countries = ['Taiwan', 'Egypt', 'US', 'Argentina', 'Romania']
# countries 2 (for image 2)
#countries = ['Egypt', 'US', 'US', 'Egypt', 'Argentina']
'''
# Country flags
for country in countries:
	ri.MakeTexture('../textures/' + country + '_flag_diff.png', '../textures/' + country + '_flag_diff.tx', 'clamp', 'clamp', 'catmull-rom', 1, 1)
	ri.MakeTexture('../textures/' + country + '_flag_translucency.png', '../textures/' + country + '_flag_translucency.tx', 'clamp', 'clamp', 'catmull-rom', 1, 1)
	
# Environment maps
ri.MakeLatLongEnvironment('../hdr/ferry_to_stockholm/hdrmaps_com_free_072_Ref.exr', '../hdr/ferry_to_stockholm/hdrmaps_com_free_072_Ref.tx', 'catmull-rom', 1, 1)

# Common maps
ri.MakeTexture('../textures/flag_nrm.png', '../textures/flag_nrm.tx', 'periodic', 'periodic', 'catmull-rom', 1, 1)
ri.MakeTexture('../textures/flag_disp.png', '../textures/flag_disp.tx', 'periodic', 'periodic', 'catmull-rom', 1, 1)
ri.MakeTexture('../textures/flag_spec.png', '../textures/flag_spec.tx', 'periodic', 'periodic', 'catmull-rom', 1, 1)
'''

############################### worldBegin() #####################################
ri.WorldBegin()

### General attributes ###
ri.Attribute('trace', {'int displacements' : 1, 'int samplemotion' : 1})
ri.ShadingInterpolation('smooth');

### Lights ###

ri.AttributeBegin() # begin lights #

# Environment light
'''
Because the 'up' vector in the PxrStdEnvMapLight shader is +Z, so we first have to rotate by X-90 to have the map in the correct (upright) position
More info: https://renderman.pixar.com/resources/current/RenderMan/envLightOrientation.html
Note: important to have it BEFORE the directlight, to avoid the directlight trying to "light" the envsphere geometry.
'''
ri.TransformBegin()
ri.Scale(1,1,-1)
ri.Rotate(-83, 1, 0, 0) # Rx
ri.Rotate(-63, 0, 0, 1) # Rz
ri.AreaLightSource('PxrStdEnvMapLight', {ri.HANDLEID : 'envmap1',
										'float exposure' : -1.7,
										'color diffAmount' : [0.7, 0.7, 0.7],
										'string rman__EnvMap' : '../hdr/ferry_to_stockholm/hdrmaps_com_free_072_Ref.tx'})
ri.Bxdf('PxrLightEmission', 'lightEmission')
ri.Geometry('envsphere')
ri.TransformEnd()

# Direct light
ri.TransformBegin()
ri.Rotate(-81, 0, 1, 0) # Ry
ri.Rotate(-23, 1, 0, 0) # Rx
ri.Rotate(0, 0, 1, 0) # Rz
ri.AreaLightSource('PxrStdAreaLight', {ri.HANDLEID : 'distLight',
										'float exposure' : 1,
										'color specAmount' : [0,0,0],
										'float temperature' : 4300})
ri.Geometry('distantlight', {'constant float anglesubtended' : 1.1}) # 1.1 for frame 1
ri.TransformEnd()

ri.AttributeEnd() # end lights #

### Geometry ###
rotations = [28.0, -25.96, -11.2, 25.2, -29.6]
tX = 1.575
colors = {'red':[1,0,0], 'green':[0,1,0], 'blue':[0,0,1], 'yellow':[1,1,0], 'violet':[0,1,1], 'gold': [1,180.0/255,0], 'white' : [1,1,1], 'black' : [0,0,0]}
flagWidth = {'US' : 1235, 'Romania' : 975, 'Argentina' : 1040, 'Taiwan' : 975, 'Egypt' : 975}
flagRotations = {'US' : -6, 'Romania' : -18, 'Argentina' : -16, 'Taiwan' : 11, 'Egypt' : -14}

for i in range(0, len(countries)):
	# hides all but the US flag for the second render
	'''
	if i not in (0, 2):
		continue
	'''
		
	ri.AttributeBegin()

	### Flags ###
	ri.AttributeBegin() # begin flag #
	'''
	Flags have different aspect ratios, so we need to take that into consideration when we apply the textures.
	By default, Renderman normalizes the texture in u, but not in v.
	Given that all the flags have normalized uv's covering the entire 0-1 uv space, we need to scale the v (or t if you like) to go from 0 to 1.
	'''
	scaleT = 650.0 / flagWidth[countries[i]] # all textures are 650px high
	offsetT = (1-scaleT)/2.0 # offset is half of the difference in t
	
	# manifold
	ri.Pattern('PxrManifold2D', 'manifold_std', {'float scaleT' : scaleT, 'float offsetT' : offsetT})
	
	# diffuse
	'''
	I intended to use a PxrRamp to create the diffuse pattern for one of the flags.
	PxrRamp does not work, neither in code nor in Maya, as an RMS node. All tests failed.
	ri.Pattern('PxrRamp', 'ramp_flag', {'float[] positions' : [0.0, 0.25, 0.5, 1.0], 'color[] colors' : [[0,0,0], [0.2,0.2,0.2], [0.5,0.5,0.5], [0.7,0.7,0.7]]})
	'''
	ri.Pattern('PxrTexture', 'tex_diffuse1', {'string filename' : '../textures/' + countries[i] + '_flag_diff.tx',
												'reference struct manifold' : 'manifold_std:result', 'int linearize' : 1})
	# translucency
	ri.Pattern('PxrTexture', 'tex_translucency1', {'string filename' : '../textures/' + countries[i] + '_flag_translucency.tx',
													'reference struct manifold' : 'manifold_std:result'})
	ri.Pattern('PxrClamp', 'clamp1', {'reference color inputRGB' : 'tex_translucency1:resultRGB', 'color min' : [0,0,0], 'color max' : [0.2,0.2,0.2]})
	
	# specularity
	uv_mult = 12
	ri.Pattern('PxrManifold2D', 'manifold_spec', {'float scaleS' : uv_mult, 'float scaleT' : scaleT * uv_mult}) # seamless texture
	ri.Pattern('PxrTexture', 'tex_spec1', {'string filename' : '../textures/flag_spec.tx', 'reference struct manifold' : 'manifold_spec:result'})
	ri.Pattern('PxrThreshold', 'tex_spec_thresh1', {'reference color inputRGB' : 'tex_spec1:resultRGB', 'float threshold' : 0.38, 'float transitionWidth' : 0.03})
	
	# bump
	ri.Pattern('PxrBump', 'tex_bump1', {'reference float inputBump' : 'tex_translucency1:resultR', 'float scale' : 0.008})
	
	# displacement
	'''
	I could not make displacement work correctly with a polygonal surface. The displacement itself is applied correctly, however the smooth normals
	of the pre-displaced mesh are discarded, resulting in a model with hard edges all over. 
	More info: http://accad.osu.edu/~smay/RManNotes/index.html (by Steve May of Pixar)
	On that page it is suggested to add the normal difference (N-Ng) to N in the displacement shader,
	but even after doing this, the model still retained a faceted look.
	
	SOLUTION: Displacement was scrapped and a simple PxrBump was used instead, which does a good job simulating detail for this particular scene.
	NOTE: The shader is found in the 'shaders' folder and to test it, simply uncomment the ri.Displacement line below.
	
	ri.Attribute('displacementbound', {'string coordinatesystem' : 'shader', 'float sphere' : 1})
	ri.Displacement('../shaders/flag_displace', {'string bumpmap' : '../textures/' + countries[i] + '_flag_translucency.tx',
												'float multiplier' : -0.1, # 0.2
												'float trueDisplacement' : 1})
	'''

	# BXDF
	ri.Bxdf('PxrLMPlastic', 'plastic1',  {'reference color diffuseColor' : 'tex_diffuse1:resultRGB',
											'float diffuseRoughness' : 0.1,
											'reference color specularColor' : 'tex_spec_thresh1:resultRGB',
											'float specularRoughness' : 0.4,
											'reference float translucence' : 'clamp1:resultR',
											'color sheen' : [1,1,1],
											'reference normal diffuseNn' : 'tex_bump1:resultN'})
	# flag geometry
	ri.Translate(tX * i, 0, 0)
	ri.Rotate(rotations[i], 0, 1, 0)
	ri.Attribute ('identifier', {'name': 'flag' + str(i+1)})
	
	# motion blur
	
	ri.MotionBegin([0, 1])
	ri.Rotate(0, 0,1,0)
	ri.Rotate(flagRotations[countries[i]] / 1.5, 0,1,0)
	ri.MotionEnd()
	
	ri.ReadArchive('flag' + str(i+1) + 'a.rib')

	ri.AttributeEnd() # end flag #

	# test sphere
	'''
	ri.TransformBegin()
	ri.Bxdf("PxrDisney", "disney_util",  {'color baseColor' : colors['black'], 'float roughness' : 0, 'float specular' : 1})
	ri.Translate(-0.5, -2.3, -1)
	ri.Sphere(1, -1, 1, 360)
	ri.TransformEnd()
	'''

	### Poles ###
	# Sphere
	ri.Bxdf("PxrLMMetal", "metal_poleSphere", {"color specularColor" : colors['gold'], "float roughness" : [ 0.2 ]})
	ri.TransformBegin()
	ri.Translate(tX * i, 3.1, 0)
	ri.Sphere(0.08, -0.1, 0.1, 360)
	ri.TransformEnd()
	
	# Pole
	ri.Bxdf("PxrDisney", "disney_pole",  {'color baseColor' : colors['white'], 'float roughness' : 0.1, 'float metallic' : 0, 'float specular' : 0.75})
	ri.TransformBegin()
	ri.Translate(tX * i, 0, 0)
	ri.Rotate(-90, 1, 0, 0)
	ri.Cylinder(0.05, -10, 3, 360)	
	ri.TransformEnd()

	ri.AttributeEnd() # end forloop iteration #

ri.WorldEnd()
############################### worldEnd() #######################################

ri.End()
