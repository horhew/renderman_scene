displacement flag_displace(string bumpmap = ""; float multiplier = 0.05; float trueDisplacement = 1;)
{
	float bumpValue = float texture(bumpmap, s, t);
    point PP = transform("shader", P);
    
    normal sN = normalize(ntransform("shader", N)); // surface normal
    normal sNg = normalize(ntransform("shader", Ng)); // geometric normal
    normal Ndiff = sN - sNg;
    
    // check if true displacement is enabled
    float enableDisp;
    evalparam("trueDisplacement", enableDisp);
    
    if (enableDisp)
    {
		// disp
		P = transform("shader", "current", PP + sN * bumpValue * multiplier);
		N = normalize(calculatenormal(P)) + ntransform("shader", "current", Ndiff);
    }
    else
    {
		// bump
		PP = transform("shader", "current", PP + sN * bumpValue * multiplier);
		N = normalize(calculatenormal(PP)) + ntransform("shader", "current", Ndiff);
	}
}
